<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = Category::all();
        $related = Product::all();
        // $product = Product::where('discount_price', '>', 30)->get();
        $product = Product::first();
        // dd($product);
        $products = Product::with('category')->get();

        // dd('got here');
        return view('shopIndex', compact('cats', 'related', 'products', 'product'));
    }

    public function product($id)
    {
        $item = Product::findOrFail($id);
        $products = Product::where('category_id', $item->id)->get();
        $related = Product::all();

        return response()->json($products);
        // return view('product', compact('related', 'products'));
    }


    public function shop()
    {
        $cats = Category::all();
        $related = Product::all();
        $product = Product::where('discount_price', '>', 300)->get();
        $categories = Category::withCount('Products')->get();
        // dd($categories);
        $products = Product::with('category')->paginate(5);

        // dd('got here');
        return view('shop', compact('cats', 'related', 'products', 'product', 'categories'));
    }
    public function show($id)
    {
        $product = Product::findOrfail($id);
        // dd($product);
        $related = Product::all();
        $cats = Category::withCount('Products')->get();


        // return response()->json([
        //     'product' => $product,
        //     'related' => $related
        // ]);

        return view('shopSingle', compact('product', 'related', 'cats'));
    }



    public function search(Request $request)
    {
        $item = $request->input('search');

        $products = Product::where('title', 'lIKE', '%' . $item . '%')->get();

        return response()->json($products);
    }


    public function cart1()
    {
        return view('cart1');
    }


    public function addToCart($id)
    {
        $product = Product::find($id);

        if (!$product) {

            abort(404);
        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if (!$cart) {

            $cart = [
                $id => [
                    "name" => $product->title,
                    "quantity" => 1,
                    "price" => $product->original_price,
                    "photo" => $product->image
                ]
            ];

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        // if cart not empty then check if this product exist then increment quantity
        if (isset($cart[$id])) {

            $cart[$id]['quantity']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        // if item not exist in cart then add to cart with quantity = 1

        $cart[$id] = [
            "name" => $product->title,
            "quantity" => 1,
            "price" => $product->original_price,
            "photo" => $product->image
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }


    public function update(Request $request)
    {
        if ($request->id && $request->quantity) {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {

            $cart = session()->get('cart');

            if (isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }
}