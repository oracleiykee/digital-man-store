<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Storage;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category')->Paginate(6);

        return view('admin.products.index', compact('products'));
        //
    }

    public function register(Request $request)
    {
        // $input = $request->all();
        // $input->save();

        // return redirect()->route('register');
    }

    public function checkout()
    {

        // return view('checkout');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', '==', '1')->get();
        $products = Product::all();
        // dd($products);

        return view('admin.Products.create', compact('categories', 'products'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'string|required',
            'description' => 'string|required',
            'original_price' => 'string|required',
            'discount_price' => 'string|required',
            'category' => 'required|exists:categories,id',
            'image' => 'required|image',
        ]);

        $imageName = $request->file('image')->store('products');

        $product = new Product(
            [
                'title' => $data['title'],
                'description' => $data['description'],
                'image' => $imageName,
                'original_price' => $data['original_price'],
                'discount_price' => $data['discount_price'],
                'category_id' => $data['category']
            ]
        );

        $product->save();

        return redirect()->route('products.index')->with('message', 'Product added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        $categories = Category::all();

        $category = Category::pluck('status', 'id');


        // $category = $product->category;


        return view('admin.products.edit', compact('product', 'category', 'categories'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->validate([
            'title' => 'string|required',
            'description' => 'string|required',
            'original_price' => 'string|required',
            'discount_price' => 'string|required',
            'related' => 'boolean|required',
            'category' => 'required|exists:categories,id',
            // 'status' => 'required|exists:categories,status',
            'image' => 'required|image'

        ]);

        $product = Product::findOrFail($id);

        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')
                ->store('products');

            Storage::delete($product->image);

            $product->image = $imagePath;
        }

        $product->title = $data['title'];
        $product->description = $data['description'];
        $product->original_price = $data['original_price'];
        $product->discount_price = $data['discount_price'];
        // $product->in_stock = $data['in_stock'];
        // $product->status = $data['status'];
        $product->category_id = $data['category'];



        $product->save();

        return redirect()->route('products.index')->with('message', 'Product updated successfully.');
        //
    }

    public function feature(Request $request, $id)
    {

        $product = Product::findOrFail($id);
        $product->update([
            'related' => $request->related
        ]);
        return redirect()->back();

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $product->delete();
        return redirect()->back();
        //
    }
}