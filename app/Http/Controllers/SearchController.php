<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class SearchController extends Controller
{
    public function index()
    {
        return view('search');
    }
    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $products = Product::where('title', 'LIKE', '%' . $request->search . "%")->get();
            if ($products) {
                foreach ($products as $key => $product) {
                    $output .= '<tr>' .
                        '<td>' . $product->id . '</td>' .
                        '<td>' . $product->title . '</td>' .
                        '<td>' . $product->description . '</td>' .
                        '<td>' . $product->discount_price . '</td>' .
                        '</tr>';
                }
                return Response($output);
            }
        }
    }
    //
}