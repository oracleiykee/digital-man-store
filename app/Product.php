<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;


    protected $dates = ['deleted_at'];
    protected $casts = ['related' => 'boolean'];



    protected $fillable = [
        'category_id',
        'title',
        'image',
        'original_price',
        'discount_price',
        'in_stock',
        'status',
        'description',
        'related',

    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}