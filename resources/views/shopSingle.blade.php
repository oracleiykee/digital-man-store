<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.headerlinks1')

</head>

<body>

    <div class="site-wrap">
        @include('includes.header1')

        <div class="site-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{asset('storage/'. $product->image)}}" alt="Image" class="img-fluid" style="height
                       :400px; width:500px">
                    </div>
                    <div class="col-md-6">
                        <h2 class="text-black">{{$product->title}}</h2>
                        <p>This is a nice product</p>
                        <p><strong class="text-primary h4">${{$product->original_price}}</strong></p>

                        <p><a href="{{url('add-to-cart', $product->id)}}" class="buy-now btn btn-sm btn-primary">Add To
                                Cart</a></p>

                    </div>
                </div>
            </div>
        </div>

        <div class="site-section block-3 site-blocks-2 bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-7 site-section-heading text-center pt-4">
                        <h2>Featured Products</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="nonloop-block-3 owl-carousel">
                            @foreach ($related as $item)
                            <div class="item">
                                <div class="block-4 text-center">
                                    <figure class="block-4-image">
                                        <img src="{{asset('storage/' . $item->image)}}" alt="Image placeholder"
                                            class="img-fluid" style="height: 300px">
                                    </figure>
                                    <div class="block-4-text p-4">
                                        <h3><a href="#">{{$item->title}}</a></h3>
                                        <p class="mb-0">Finding perfect t-shirt</p>
                                        <p class="text-primary font-weight-bold">${{$item->original_price}}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="site-footer border-top">
            @include('includes.footer1')
        </footer>
    </div>
    @include('includes.footerlinks1')

    @yield('scripts')

</body>

</html>
