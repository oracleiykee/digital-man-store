@extends('layouts.shoppers')

@section('content')
{{--
<div class="site-section">

    <div class="container">

        <div class="row mb-5">
            @foreach ($products as $product)

            <div class="col-md-6 order-2">


                <div class="block-4 text-center border">
                    <figure class="block-4-image">
                        <a href="shop-single.html">

                            <img src="{{asset('storage/'. $product->image)}}" alt="Image" class="img-fluid"
style="height:200px; width:200px " class="img-fluid">
</a>
</figure>
<div class="block-4-text p-4">
    <h3 class="text-black">{{$product->title}}</h3>

    <p class="mb-0">Finding perfect t-shirt</p>
    <p><strong class="text-primary h4">${{$product->original_price}}</strong></p>
    <p>
        <a href="{{url('add-to-cart', $product->id)}}" class="buy-now btn btn-sm btn-primary">Add To
            Cart
        </a>
    </p>

</div>
</div>

</div>
@endforeach

</div>


</div>

</div> --}}


<div class="site-section">
    @foreach ($products as $product)

    <div class="container">

        <div class="row mb-5">
            <div class="col-md-6 ">
                <img src="{{asset('storage/'. $product->image)}}" alt="Image" class="img-fluid"
                    style="height:400px; width:500px " />
            </div>

            <div class=" col-md-6">
                <h2 class="text-black">{{$product->title}}</h2>
                <p>This is a nice product</p>
                <p><strong class="text-primary h4">${{$product->original_price}}</strong></p>

                <p>
                    <a href="{{url('add-to-cart', $product->id)}}" class="buy-now btn btn-sm btn-primary">Add To
                        Cart
                    </a>
                </p>

            </div>
            &nbsp;&nbsp;
        </div>

    </div>
    @endforeach

</div>
@endsection
