@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        @foreach ($products as $id => $product)

        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">{{$product->category->name}}</div>
                <div class="panel-body"><img src="{{asset('storage/' . $product->image)}}" class="img-responsive"
                        style="width:100%" alt="Image"></div>
                <h3 class="pad">{{$product->title}}</h3>
                <p class="pad" style="text-decoration:line-through">#{{$product->original_price}}</p>
                <p class="pad">#{{$product->discount_price}}</p>
                {{-- <a href=""> --}}
                <div class="panel-footer"><a href="{{ url('add-to-cart/'.$product->id) }}"><button id="{{$id}}"
                            class="btn btn-primary centa add-to-cart">Add to cart
                        </button></a>
                </div>

                {{-- </a> --}}
            </div>
        </div>
        @endforeach

    </div>
    <br><br>
    {{-- <div class="center">{{$products->links()}} --}}
</div>
</div>
@include('includes.footerpub')
@endsection
