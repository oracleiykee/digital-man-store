<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>E-commerce website by Oracle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset("bstrap/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/font-awesome/css/font-awesome.min.css')}}">
    <script src="{{asset("bstrap/css/odd.css")}}"></script>
    <script src="{{asset("bstrap/js/jquery-2.2.0.min.js")}}"></script>
    <script src="{{asset("bstrap/js/popper.min.js")}}"></script>
    <script src="{{asset("bstrap/js/bootstrap.min.js")}}"></script>
    {{-- <script src="{{asset("bstrap/js/popper.min.js")}}"></script> --}}
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 20;
        }

        body {
            margin: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
            background-image: url('assets/img/examples/product3.jpg');
            background-repeat: no-repeat;
            background-size: 100%;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            /* background-color: #f2f2f2; */
            background-color: rgba(0, 1, 2, 0.4);
            padding: 5px;
            border-radius: 15%;
        }

        .panel-footer {
            /* margin-left: 40%; */
            text-align: center;
        }

        .navbar {
            background-color: rgba(0, 0, 0, 0.6);
            border: none;
        }

        .navbar-inverse .navbar-nav a,
        /* .navbar-inverse .navbar-nav a:focus, */
        .navbar .navbar-nav a:hover {
            color: greenyellow;
            /* background-color: aquamarine; */
        }

        .pad {
            margin-left: 20px;
        }

        .center {
            text-align: center
        }

        .centa {
            /* margin-left: 40%; */
            margin-bottom: 20px
        }
    </style>
</head>

<body>

    <div class="jumbotron">
        <div class="container text-center">
            <h1>Online Store</h1>
            <p>Mission, Vission & Values</p>
        </div>
    </div>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Logo</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Deals</a></li>
                    <li><a href="#">Stores</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Your Account</a></li>
                    <li>
                        {{-- <div class="container"> --}}
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-12 main-section">
                                <div class="dropdown">
                                    <button type="button" class="btn btn-info" data-toggle="dropdown">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span
                                            class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                                    </button>
                                    <div class="dropdown-menu" style="min-width:300px;">
                                        <div class="row total-header-section">
                                            <div class="col-lg-6 col-sm-6 col-6">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span
                                                    class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                                            </div>

                                            <?php $total = 0 ?>
                                            @foreach((array) session('cart') as $id => $details)
                                            <?php $total += $details['price'] * $details['quantity'] ?>
                                            @endforeach

                                            <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                                <p>Total: <span class="text-info">$ {{ $total }}</span></p>
                                            </div>
                                        </div>

                                        @if(session('cart'))
                                        @foreach(session('cart') as $id => $details)
                                        <div class="row cart-detail">
                                            <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                                <img src="{{asset('storage/'.$details['photo']) }}" width="50"
                                                    height="50" />
                                            </div>
                                            <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                                <p>{{ $details['name'] }}</p>
                                                <span class="price text-info"> ${{ $details['price'] }}</span> <span
                                                    class="count">
                                                    Quantity:{{ $details['quantity'] }}</span>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                        <div class="row">
                                            <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                                                <a href="{{ url('cart') }}" class="btn btn-primary btn-block">View
                                                    all</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- </div> --}}
                    </li>
                    {{-- <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>
                            Cart<span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                    </a></li> --}}
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')


    @yield('scripts')
</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_store&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 13 Jun 2016 16:00:20 GMT -->

</html>
