<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.headerlinks')
</head>

<body data-background-color="dark">
    <div class="wrapper">
        <div class="main-header">
            <!-- Logo Header -->
            @include('includes.logoheader')
            <!-- End Logo Header -->

            <!-- Navbar Header -->
            @include('includes.navbar')
            <!-- End Navbar -->
        </div>

        <!-- Sidebar -->
        @include('includes.sidebar')
        <!-- End Sidebar -->

        @yield('content')

        @include('includes.footer')
    </div>

    <!-- Custom template | don't include it in your project! -->
    @include('includes.customtemplate')
    <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    @include('includes.footerlinks')
</body>

</html>
