<!DOCTYPE html>
<html lang="en">

@include('includes.headerlinks1')

<body>

    @include('includes.header1')

    @yield('content')



    @include('includes.footer1')
    @include('includes.footerlinks1')
    <script>
        function htmlContainer(url, asset, item, url1) {
        const content = `
        <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
            <div class="block-4 text-center border">
                <figure class="block-4-image">
                    <a href="${url}">
                        <img src="${asset}" alt="Image placeholder" class="img-fluid" style="height:200px">
                    </a>
                </figure>
                <div class="block-4-text p-4">
                    <h3>${item.title}</h3>
                    <p class="mb-0">Finding perfect products</p>
                    <p class="text-primary font-weight-bold">${item.discount_price}</p></a>

                    <a href="${url1}">
                        <button id="" class="btn btn-primary btn-sm centa add-to-cart">Add to cart
                        </button>
                    </a>
                </div>
            </div>
        </div>`
        return content;
        }
    </script>
    @yield('scripts')
</body>

</html>
