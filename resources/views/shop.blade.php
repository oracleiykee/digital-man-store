@extends('layouts.shoppers')

@section('content')

<div class="site-wrap">
    {{-- @include('shoppers.header') --}}
    <div class="bg-light py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-0"><a href="{{url('shoppers')}}">Home</a> <span class="mx-2 mb-0">/</span>
                    <strong class="text-black">Shop</strong></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="site-section">

            <div class="row mb-5">
                <div class="col-md-9 order-2">

                    <div class="row">
                        <div class="col-md-12 mb-5">
                            <div class="float-md-left mb-4">
                                <h2 class="text-black h5">Shop All</h2>
                            </div>
                            <div class="d-flex">
                                <div class="dropdown mr-1 ml-md-auto">
                                    <button type="button" class="btn btn-secondary btn-sm dropdown-toggle"
                                        id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                        Latest
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                                        @foreach ($categories as $cat)

                                        <a class="dropdown-item cat" href="">{{$cat->name}}
                                            <input type="hidden" value="{{$cat->id}}" />

                                        </a>
                                        @endforeach

                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-secondary btn-sm dropdown-toggle"
                                        id="dropdownMenuReference" data-toggle="dropdown">Reference</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference">
                                        <a class="dropdown-item" href="#">Relevance</a>
                                        <a class="dropdown-item" href="#">Name, A to Z</a>
                                        <a class="dropdown-item" href="#">Name, Z to A</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Price, low to high</a>
                                        <a class="dropdown-item" href="#">Price, high to low</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-5" id="products">

                        @foreach ($products as $product)
                        <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                            <div class="block-4 text-center border">
                                <figure class="block-4-image">
                                    <a href="{{ route('show', $product->id) }}"><img
                                            src="{{asset('storage/'. $product->image)}}" alt="Image placeholder"
                                            class="img-fluid" style="height:200px;">
                                </figure>
                                <div class="block-4-text p-4">
                                    <h3>{{$product->title}}</h3>
                                    <p class="mb-0">Finding perfect products</p>
                                    <p class="text-primary font-weight-bold">${{$product->original_price}}</p></a>
                                    <a href="{{url('add-to-cart', $product->id)}}">
                                        <button id="" class="btn btn-primary btn-sm centa add-to-cart">Add to cart
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                    <div class="row" data-aos="fade-up">
                        <div class="col-md-12 text-center">
                            <div class="site-block-27">
                                <ul style="margin-left:40%">
                                    {{$products->links()}}

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 order-1 mb-5 mb-md-0">
                    <div class="border p-4 rounded mb-4">
                        <h3 class="mb-3 h6 text-uppercase text-black d-block">Categories</h3>
                        <ul class="list-unstyled mb-0">
                            @foreach ($categories as $count)
                            <li class="mb-1">
                                {{-- <a href="{{url('product', $count->id)}}" class="d-flex cat"> --}}

                                <a href="" class="d-flex cat">
                                    <input type="hidden" value="{{$count->id}}" />
                                    <span>{{$count->name}}</span>
                                    <span class="text-black ml-auto">{{$count->products_count}}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>

                    </div>

                    <div class="border p-4 rounded mb-4">
                        <div class="mb-4">
                            <h3 class="mb-3 h6 text-uppercase text-black d-block">Filter by Price</h3>
                            <div id="slider-range" class="border-primary"></div>
                            <input type="text" name="text" id="amount" class="form-control border-0 pl-0 bg-white"
                                disabled="" />
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="site-section site-blocks-2">
                        <div class="row justify-content-center text-center mb-5">
                            <div class="col-md-7 site-section-heading pt-4">
                                <h2>Categories</h2>
                            </div>
                        </div>
                        <div class="row">
                            @foreach ($cats as $cat)
                            <div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0" data-aos="fade" data-aos-delay="">
                                <a class="block-2-item" href="#">
                                    <figure class="image">
                                        <img src="{{asset('storage/'. $cat->image)}}" alt="" class="img-fluid"
                                            style="height:330px">
                                    </figure>
                                    <div class="text">
                                        <span class="text-uppercase">Collections</span>
                                        <h3>{{$cat->name}}</h3>
                                    </div>
                                </a>
                            </div>
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
@yield('scripts')
<script>
    // script for latest


// latest link script end

// category link script begin
    $(document).ready(function(){
        $('.cat').on("click",function(e){
            e.preventDefault();
            const id = $(this).find('input').val();
            let url = "{{ route('product', '%id%')}}" ;
            url = url.replace('%id%', id)

            $.ajax({
                url: url,
                method: "GET",
                data:{
                    id: id,
                },
                success: function (response) {
                    let output = ''
                    // console.log(response)
                    response.forEach(item => {
                    let url = "{{ route('show', '%id%')}}" ;
                    url = url.replace('%id%', item.id)
                    let asset = "{{ asset('storage/%image%')}}"
                    asset = asset.replace('%image%', item.image)

                    let url1 = "{{ route('add', '%id%')}}"
                    url1 = url1.replace('%id%', item.id)
                    // console.log(url)
                    let container = htmlContainer(url, asset, item, url1)

                    output += container;
                    });
                    $('#products').html(output);
                    console.log('Response ',response)
                }

            })
        })
    })

    // category link script ends
</script>
@endsection
