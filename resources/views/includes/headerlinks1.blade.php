<title>The Trends &mdash; Online Store</title>
<meta name="_token" content="{{ csrf_token() }}">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
<link rel="stylesheet" href="{{asset('assetShop/fonts/icomoon/style.css')}}">

<link rel="stylesheet" href="{{asset('assetShop/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assetShop/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('assetShop/css/jquery-ui.css')}}">
<link rel="stylesheet" href="{{asset('assetShop/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assetShop/css/owl.theme.default.min.css')}}">
<script src="{{asset('bstrap/js/jquery-2.2.0.min.js')}}"></script>
{{-- <link rel="stylesheet" href="{{asset('bstrap/js/jquery-2.2.0.min.js')}}"> --}}


<link rel="stylesheet" href="{{asset('assetShop/css/aos.css')}}">

<link rel="stylesheet" href="{{asset('assetShop/css/style.css')}}">
