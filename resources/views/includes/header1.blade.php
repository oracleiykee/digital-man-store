<header class="site-navbar" role="banner">
    <div class="site-navbar-top">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
                    <form action="{{url('search')}}" method="post" class="site-block-top-search">
                        {{ csrf_field() }}
                        <span class="icon icon-search2"></span>
                        <input type="text" name="search" class="form-control border-0 search" id="search"
                            placeholder="Search">
                    </form>

                </div>

                <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
                    <div class="site-logo">
                        <a href="index.html" class="js-logo-clone">Digital man</a>
                    </div>
                </div>

                <div class="col-6 col-md-4 order-3 order-md-3 text-right">
                    <div class="site-top-icons">
                        <ul>
                            <li><a href="#"><span class="icon icon-person"></span></a></li>
                            <li><a href="#"><span class="icon icon-heart-o"></span></a></li>
                            <li>
                                <a href="{{url('cart1')}}" class="site-cart">
                                    <span class="icon icon-shopping_cart"></span>
                                    <span class="count">{{ count((array) session('cart')) }}</span>
                                </a>
                            </li>
                            <li class="d-inline-block d-md-none ml-md-0"><a href="#"
                                    class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
            <ul class="site-menu js-clone-nav d-none d-md-block">
                <li class=" active">
                    {{-- <li class="has-children active"> --}}
                    <a href="{{url('shoppers')}}">Home</a>
                    {{-- <ul class="dropdown">
                        <li><a href="#">Menu One</a></li>
                        <li><a href="#">Menu Two</a></li>
                        <li><a href="#">Menu Three</a></li>
                        <li class="has-children">
                            <a href="#">Sub Menu</a>
                            <ul class="dropdown">
                                <li><a href="#">Menu One</a></li>
                                <li><a href="#">Menu Two</a></li>
                                <li><a href="#">Menu Three</a></li>
                            </ul>
                        </li>
                    </ul> --}}
                </li>
                {{-- <li class="has-children"> --}}
                <li class="">
                    <a href="about.html">About</a>
                    {{-- <ul class="dropdown">
                        <li><a href="#">Menu One</a></li>
                        <li><a href="#">Menu Two</a></li>
                        <li><a href="#">Menu Three</a></li>
                    </ul> --}}
                </li>
                <li><a href="{{url('shop')}}">Shop</a></li>
                {{-- <li><a href="#">Catalogue</a></li>
                <li><a href="#">New Arrivals</a></li> --}}
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </div>
    </nav>
</header>

@section('scripts')
<script>
    $(document).ready(function() {
        $("#search").on("keyup", function (e) {

            e.preventDefault();
           $search=$(this).val();
            $.ajax({
            url: '{{ url('search') }}',
            method: "POST",
            data:{'search': $search,
            _token : '{{ csrf_token() }}'},
            success: function (response) {
                console.log('Response from ajax ',response)
                let output = ''


                response.forEach(item => {
                    let url = "{{ route('show', '%id%')}}" ;
                    url = url.replace('%id%', item.id)
                    let asset = "{{ asset('storage/%image%')}}"
                    asset = asset.replace('%image%', item.image)

                    let url1 = "{{ route('add', '%id%')}}"
                    url1 = url1.replace('%id%', item.id)
                    // console.log(url)
                    let container = `
                        <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                            <div class="block-4 text-center border">
                                <figure class="block-4-image">
                                    <a href="${url}">
                                    <img src="${asset}"
                                            alt="Image placeholder" class="img-fluid" style= "height:200px">
                                        </a>
                                </figure>
                                <div class="block-4-text p-4">
                                    <h3>${item.title}</h3>
                                    <p class="mb-0">Finding perfect products</p>
                                    <p class="text-primary font-weight-bold">${item.discount_price}</p></a>

                                    <a href="${url1}">
                                        <button id="" class="btn btn-primary btn-sm centa add-to-cart">Add to cart
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    `
                    output += container;
                });
                $('#products').html(output);
                console.log(response)

            }

            });
        });
    })

</script>
@endsection
