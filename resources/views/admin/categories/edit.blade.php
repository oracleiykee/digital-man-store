@extends('layouts.admin')
@section('content')

<div class="main-panel">
    <div class="content">
        <div class="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Add Category</div>
                        </div>
                        <div class="card-body">
                            <form action="categories.store" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <label for="email2">Categories</label>
                                            <input type="email" class="form-control" name="name" id="email2"
                                                placeholder="Enter category">
                                            <small id="emailHelp2" class="form-text text-muted">We'll never share your
                                                email
                                                with anyone else.</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Status</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>1</option>
                                                <option>2</option>

                                            </select>
                                        </div>


                                    </div>
                                </div>
                                <div class="card-action">
                                    <button class="btn btn-success">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endsection
