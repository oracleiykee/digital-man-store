@extends('layouts.admin')
@section('content')

<div class="main-panel">
    <div class="content">
        <div class="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Add Category</div>
                        </div>
                        @if($errors->any())
                        @foreach ($errors->all() as $error)
                        {{ $error }}
                        @endforeach
                        @endif
                        <div class="card-body">
                            <form action="{{route('categories.store')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <label for="email2">Categories</label>
                                            <input type="text" class="form-control" name="name" id="email2"
                                                placeholder="Enter category">

                                        </div>

                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Status</label>
                                            <select required class="form-control" name="status"
                                                id="exampleFormControlSelect1">
                                                <option value=" 0" selected>0</option>
                                                <option value=" 1 "> 1 </option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="email2">Image</label>
                                            <input type="file" class="form-control" name="image" id="email2"
                                                placeholder="choose image" required>

                                        </div>

                                    </div>
                                </div>
                                <div class="card-action">
                                    <button class="btn btn-success">Submit</button>
                                </div>
                            </form>
                        </div>
                        <hr style="background-color:red">
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title">Category Table</div>
                                    </div>
                                    <div class="card-body">

                                        <table class="table mt-3">
                                            <thead>
                                                <tr>
                                                    <th scope="col">S/N</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">status</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($categories as $i => $category)
                                                <tr>
                                                    <td>{{$i + 1}}</td>
                                                    <td>{{$category->name}}</td>
                                                    <td>
                                                        @if($category->status == 1)
                                                        <form action="{{route('categories.update', $category->id)}}"
                                                            method="post">
                                                            {{csrf_field()}}
                                                            {{method_field('PUT')}}
                                                            <input type="hidden" name="status" value="0">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                    class="btn btn-sm btn-info">Enable</button>
                                                            </div>
                                                        </form>
                                                        @else
                                                        <form action="{{route('categories.update', $category->id)}}"
                                                            method="post">
                                                            {{csrf_field()}}
                                                            {{method_field('PUT')}}
                                                            <input type="hidden" name="status" value="1">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                    class="btn btn-sm btn-success">Dissable</button>
                                                            </div>
                                                        </form>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="center" style="margin-left:40%">
                                    {{-- {{$products->links()}} --}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    @endsection
    {{-- @section('scripts')
    <script>
        $(function() {
        $('.toggle-class').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var user_id = $(this).data('id');

            $.ajax({
                type: "GET",
                dataType: "json",
                url: '/changeStatus',
                data: {'status': status, 'id': id},
                success: function(data){
                  console.log(data.success)
                }
            });
        })
      })
    </script>
    @endsection --}}

    {{-- {{ $order->quantity == 1 ? 'selected' : '' }} --}}

    {{-- {{-- <input data-id="{{$category->id}}" class="toggle-class"
    type="checkbox" data-onstyle="success"
    data-offstyle="danger" data-toggle="toggle" data-on="Active"
    data-off="InActive"
    {{ $category->status ? 'checked' : '' }}
    </td>

    </tr>
    @endforeach --}}
