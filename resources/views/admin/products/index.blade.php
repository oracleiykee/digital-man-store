@extends('layouts.admin')
@section('content')

<div class="main-panel">
    <div class="content">
        <div class="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Products Table</div>
                        </div>
                        <div class="card-body">

                            <table class="table mt-3">
                                <thead>
                                    <tr>
                                        <th scope="col">S/N</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Category name</th>
                                        <th scope="col">Original Price</th>
                                        <th scope="col">Discounted Price</th>
                                        <th scope="col">actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $i => $product)
                                    <tr>
                                        <td>{{$i + 1}}</td>
                                        <td>{{$product->title}}</td>
                                        <td>{{$product->category->name}}</td>
                                        <td>{{$product->original_price}}</td>
                                        <td>{{$product->discount_price}}</td>
                                        <td>
                                            <form action="{{route('products.destroy', $product->id)}}" method="post">
                                                {{ csrf_field() }}
                                                {{method_field('DELETE')}}
                                                <button type="submit" class="btn btn-sm btn-danger">X
                                                    {{-- <i class="flaticon-trash"> &#xE254; </i> --}}
                                                </button>
                                            </form>
                                        </td>

                                        <td>
                                            <a href="{{route('products.edit', $product->id)}}"><i
                                                    class="flaticon-pen"></i></a>
                                        </td>

                                        <td>
                                            @if($product->related == 1)
                                            <form action="{{route('prod.update', $product->id)}}" method="post">
                                                {{ csrf_field() }}

                                                {{method_field('PUT')}}
                                                <input type="hidden" name="related" value="0">
                                                <button type="submit" class="btn btn-sm btn-success">ON</button>
                                            </form>
                                            @else
                                            <form action="{{route('prod.update', $product->id)}}" method="POST">
                                                {{ csrf_field() }}
                                                {{method_field('PUT')}}
                                                <input type="hidden" name="related" value="1">
                                                <button type="submit" class="btn btn-sm btn-danger">OFF</button>
                                            </form>
                                            @endif
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="center" style="margin-left:40%">
                        {{$products->links()}}
                    </div>
                </div>


            </div>
        </div>
    </div>

</div>
@endsection
