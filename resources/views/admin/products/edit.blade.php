@extends('layouts.admin')
@section('content')

<div class="main-panel">
    <div class="content">
        <div class="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">Edit Products</div>
                        </div>
                        <div class="card-body">
                            <form action="{{route('products.update', $product->id)}}" method="post"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{method_field('PUT')}}
                                <div class="row">
                                    <div class="col-md-6 col-lg-4">

                                        <div class="form-group">
                                            <label for="email2">title</label>
                                            <input type="text" class="form-control" name="title" id="email2"
                                                placeholder="Title" value="{{$product->title}}">

                                        </div>
                                        <div class="form-group">
                                            <label for="email2">Description</label>
                                            <input type="text" class="form-control" name="description" id="email2"
                                                placeholder="">

                                        </div>

                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Choose category</label>
                                            <select required class="form-control" name="category" title="Category"
                                                id="exampleFormControlSelect1">
                                                @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="email2">Image</label>
                                            <input type="file" class="form-control" name="image" id="email2"
                                                placeholder="choose image" required
                                                value="{{asset('storage/' . $product->image)}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email2">Original price</label>
                                            <input type="text" class="form-control" name="original_price" id="email2"
                                                placeholder="Enter original price" value="{{$product->original_price}}">

                                        </div>
                                        <div class="form-group">
                                            <label for="email2">Discount price</label>
                                            <input type="text" class="form-control" name="discount_price" id="email2"
                                                placeholder="Enter Discount price" value="{{$product->discount_price}}">

                                        </div>



                                    </div>
                                </div>
                                <div class="card-action">
                                    <button class="btn btn-success">Submit</button>
                                </div>
                            </form>

                        </div>


                    </div>
                </div>

                @endsection
