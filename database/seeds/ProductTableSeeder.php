<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Branded Foods', 'Households', 'Vegies and Fruits', 'Kitchen', 'Bread and bakery'];
        foreach ($categories as $category) {
            $actual_price = 100;
            $discount_price = 97;
            DB::table('products')->insert([
                'title' => 'product for ' . $category[0]->name,
                'image' => '',
                'category_id' => $category->id,
                'original_price' => $actual_price,
                'discount_price' => $discount_price,
                'in_stock' => 1,
                'status' => 1,
                'created_at' => Carbon::now(),
            ]);
        }
        //
    }
}