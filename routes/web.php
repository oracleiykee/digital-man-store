<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\Http\Controllers\PublicController;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SearchController;

Route::get('/', function () {
    return view('welcome');
});
// Route::get('admin', 'AdminHomeController@index')->name('admin.dashboard');

Route::resource('products', 'ProductsController');
Route::resource('categories', 'CategoriesController');
Route::resource('index', 'PublicController');
Route::resource('admin', 'AdminHomeController');
Route::put('products/{id}/feature', 'ProductsController@feature')->name('prod.update');
Route::get('product/{id}', 'HomeController@product')->name('product');
Route::post('search', 'HomeController@search')->name('search');
Route::post('home', 'HomeController@searchHome')->name('home');
Route::get('checkout', 'ProductsController@checkout')->name('checkout');
Route::get('register', 'ProductsController@register')->name('register');


Route::get('/shoppers', 'HomeController@index');
Route::get('/ind', 'PublicController@index');
Route::get('/shop', 'HomeController@shop');
Route::get('/show/{id}', 'HomeController@show')->name('show');
Route::get('/cart1', 'HomeController@cart1');
Route::get('add-to-cart/{id}', 'HomeController@addToCart')->name('add');
Route::patch('update-cart', 'HomeController@update');
Route::delete('remove', 'HomeController@remove');


Route::get('/search', 'SearchController@search');
Route::get('/index', 'SearchController@index');


// Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');